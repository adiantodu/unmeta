import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import $ from 'jquery';

import filter_ico from "../../../assets/filter-ico.svg"

import nft1 from "../../../assets/nft-2.png"
import ava1 from "../../../assets/ava-1.png"

import love_ico from "../../../assets/love-ico.svg"
import love_ico_active from "../../../assets/love-ico-active.svg"


function likeThis(e){
    $(e.currentTarget).toggleClass('active')
}

function Popular_panel(){
    var settings = {
        slidesToShow: 3,
        slidesToScroll: 1,
        variableWidth: false,
        arrows: false,
    };
    return(
        <div className="popular-panel">
            <div className="popular-top flex-r just-space v-center">
                <span className="t-500 t-18">
                    Popular
                </span>
                <button className="popular-filter cursor">
                    <fieldset>
                        <legend className="t-10 t-500">Filter</legend>
                        <div className="flex-r v-center t-12 t-500">
                            <img src={filter_ico} alt=""/> All NFTs
                        </div>
                    </fieldset>
                </button>
            </div>
                <Slider {...settings} className="popular-content">
                    {[1,2,3,4,5,6].map((s)=>{
                        return (
                            <div key={s}>
                                <div className="popular-itm">
                                    <div className="popular-itm-top flex-r v-center just-space">
                                        <span className="t-13 t-500 line-clamp clamp-1">
                                            NFT Collection
                                        </span>
                                        <img src={ava1} alt=""/>
                                    </div>
                                    <div className="popular-itm-img">
                                        <img src={nft1} alt=""/>
                                        <button className="flex-r v-center h-center cursor" onClick={(e) => likeThis(e)}>
                                            <img src={love_ico} alt=""/>
                                            <img src={love_ico_active} alt=""/>
                                        </button>
                                        <label className="t-13 t-500 line-clamp clamp-1">
                                            Virtual Worlds
                                        </label>
                                    </div>
                                    <div className="popular-itm-detail flex-r v-center just-space">
                                        <div className="flex-c h-center">
                                            <span className="t-14 t-500">
                                                10K
                                            </span>
                                            <span className="t-13">
                                                Collection
                                            </span>
                                        </div>
                                        <div className="flex-c h-center">
                                            <span className="t-14 t-500">
                                                145K
                                            </span>
                                            <span className="t-13">
                                                Price
                                            </span>
                                        </div>
                                        <div className="flex-c h-center">
                                            <span className="t-14 t-500">
                                                120K
                                            </span>
                                            <span className="t-13">
                                                Followers
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )
                    })}
                </Slider>
        </div>
    )
}

export default Popular_panel