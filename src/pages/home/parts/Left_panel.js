import menu1 from "../../../assets/menu-1.svg"
import menu2 from "../../../assets/menu-2.svg"
import menu3 from "../../../assets/menu-3.svg"
import menu4 from "../../../assets/menu-4.svg"
import menu5 from "../../../assets/menu-5.svg"
import menu6 from "../../../assets/menu-6.svg"


function Left_panel() {
    return (
        <div className="left-panel flex-c">
            <a href="" className="t-500 flex-r v-center active t-17">
                <img src={menu1} alt=""/>
                Explore
            </a>
            <a href="" className="t-500 flex-r v-center t-17">
                <img src={menu2} alt=""/>
                Marketplace
            </a>
            <a href="" className="t-500 flex-r v-center t-17">
                <img src={menu3} alt=""/>
                Exchange
            </a>
            <a href="" className="t-500 flex-r v-center t-17">
                <img src={menu4} alt=""/>
                Statistics
            </a>
            <a href="" className="t-500 flex-r v-center t-17">
                <img src={menu5} alt=""/>
                Help Center
            </a>
            <a href="" className="t-500 flex-r v-center t-17">
                <img src={menu6} alt=""/>
                Settings
            </a>


            <a href="" className="float-create-btn flex-r v-center h-center t-500 t-14">
                Create
            </a>
        </div>
    );
}

export default Left_panel;