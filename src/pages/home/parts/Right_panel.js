

function Right_panel() {
    return (
        <div className="right-panel">
            <div className="inner-panel flex-c">
                <div className="panel-title flex-r v-center just-space">
                    <span className="t-18 t-500">
                        Trending
                    </span>
                    <a className="t-14 t-500">
                        All NFTs
                    </a>
                </div>
                <div className="panel-content">

                    {[1,2,3,4,5,6,7,8].map(()=>{
                        return(
                            <a href="" className="flex-r v-center">
                                <img src="" alt=""/>
                                <div className="flex-c">
                                    <div className="top t-12 t-500">
                                        CryptoPunks
                                    </div>
                                    <div className="btm flex-r just-space v-center">
                                        <span className="t-12 t-500">
                                            Collectibles
                                        </span>
                                        <span className="t-12 t-500">
                                            14,177.37
                                        </span>
                                    </div>
                                </div>
                            </a>
                        )
                    })}


                </div>
            </div>


            <div className="inner-panel flex-c">
                <div className="panel-title flex-r v-center just-space">
                    <span className="t-18 t-500">
                        Upcoming
                    </span>
                    <a className="t-14 t-500">
                        Today
                    </a>
                </div>
                <div className="panel-content">

                    {[1,2,3,4,5,6,7,8].map((s)=>{
                        return(
                            <a href="" key={s} className="flex-r v-center">
                                <img src="" alt=""/>
                                <div className="flex-c">
                                    <div className="top t-12 t-500">
                                        CryptoPunks
                                    </div>
                                    <div className="btm flex-r just-space v-center">
                                        <span className="t-12 t-500">
                                            Collectibles
                                        </span>
                                        <span className="t-12 t-500">
                                            12 mins
                                        </span>
                                    </div>
                                </div>
                            </a>
                        )
                    })}


                </div>
            </div>
        </div>
    );
}

export default Right_panel;



