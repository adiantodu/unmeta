import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";


import filter_ico from "../../../assets/filter-ico.svg"
import nft3 from "../../../assets/nft-3.png"
import ava1 from "../../../assets/ava-1.png"
import love_ico from "../../../assets/love-ico.svg"
import love_ico_active from "../../../assets/love-ico-active.svg"

import money_ico from "../../../assets/money-ico.svg"





function Spotlight_panel(){
    var settings = {
        infinite: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        variableWidth: false,
        arrows: false,
    };
    return(
        <div className="spotlight-panel">
            <div className="spotlight-title flex-r v-center just-space">
                <span className="t-18 t-500">
                    Creator Spotlight
                </span>
                <button className="spotlight-filter cursor">
                    <fieldset>
                        <legend className="t-10 t-500">Filter</legend>
                        <div className="flex-r v-center t-12 t-500">
                            <img src={filter_ico} alt=""/> All NFTs
                        </div>
                    </fieldset>
                </button>
            </div>
            <div className="spotlight-content flex-r v-btm">
                <div className="spotlight-content-left">
                    <div className="spotlight-content-name t-28 t-500">
                        Jonathan John
                    </div>
                    <div className="flex-r v-center spotlight-content-follow">
                        <div className="flex-c h-center t-13 t-500">
                            Followers
                            <span className="t-20 t-500">
                                100 K
                            </span>
                        </div>
                        <div className="flex-c h-center t-13 t-500">
                            Followers
                            <span className="t-20 t-500">
                                100 K
                            </span>
                        </div>
                    </div>
                    <Slider {...settings} className="spotlight-content-slider">
                        {[1,2,3,4,5].map((s)=>{
                            return(
                                <div key={s}>
                                    <div className="spotlight-content-itm flex-r v-center h-center">
                                        <img src={nft3} alt=""/>
                                        <div className="flex-r v-center">
                                            <img src={ava1} alt="" />
                                            <span className="t-13 t-500">
                                                Floyd Miles
                                            </span>
                                        </div>
                                    </div> 
                                </div>
                            )
                        })}
                    </Slider>
                </div>
                <div className="spotlight-content-right">
                    <div className="flex-r">
                        <img src={nft3} alt="" className="img-bg"/>
                        <div className="flex-r v-center h-center love-ico cursor">
                            <img src={love_ico} alt=""/>
                        </div>
                        <div className="user-label flex-r v-center">
                            <img src="" alt=""/>
                            <span className="line-clamp clamp-1 t-12">
                                Jonathan John
                            </span>
                        </div>
                        <div className="title-label flex-r v-center just-space t-13">
                            <div className="flex-c v-center">
                                <span>
                                    Crypto Punk
                                </span>
                                <div className="flex-r v-center">
                                    <img src={money_ico} alt=""/>
                                    100K
                                </div>
                            </div>
                            <button>
                                Place a bid
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Spotlight_panel