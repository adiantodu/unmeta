function Top_banner(){
    return(
        <div className="top-banner">
            <h2 className="t-32 t-500">
                Collect, Create, and sell NFTs
            </h2>
            <div className="top-banner-warp flex-r v-center t-14 t-500">
                <a href="">
                    Collect
                </a>
                <a href="">
                    Create
                </a>
            </div>
        </div>
    )
}

export default Top_banner