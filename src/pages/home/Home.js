import React from "react";
import $ from 'jquery';

import Navbar from "../../components/navbar/Navbar";
import Right_panel from "./parts/Right_panel"
import Left_panel from "./parts/Left_panel"
import Top_banner from "./parts/Top_banner";
import Popular_panel from "./parts/Popular_panel";
import Spotlight_panel from "./parts/Spotlight_panel";



export default class Home extends React.Component{
    render(){
        require('./Home.css')
        return(
            <>
                
                <Navbar></Navbar>

                <div className="section home-sec1">
                    <div className="container home-sec1-c">

                        <Left_panel></Left_panel>
                        
                        <div className="center-panel">

                            <Top_banner></Top_banner>
                            
                            <Popular_panel></Popular_panel>

                            <Spotlight_panel></Spotlight_panel>    

                            <div>
                            </div>                        

                        </div>

                        <Right_panel></Right_panel>
                        
                    </div>
                </div>
            </>
        )
    }
}