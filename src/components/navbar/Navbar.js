import './Navbar.css';
import logo from "../../assets/logo.svg"
import search_ico from "../../assets/search-ico.svg"
import wallet_ico from "../../assets/wallet-ico.svg"
import notif_ico from "../../assets/notif-ico.svg"
import chat_ico from "../../assets/chat-ico.svg"

function Navbar() {
    return (
        <div className="section nav flex-c v-center">
            <div className="container flex-r v-center">
                <a href="" className='nav-logo flex-r v-center h-center'>
                    <img src={logo} alt=""/>
                </a>

                <div className='nav-search flex-r v-center'>
                    <img src={search_ico} alt=""/>
                    <input type="text" placeholder="Collection, item or user"/>
                </div>

                <div className='nav-menu flex-r v-center h-right'>
                    <a href="" className='flex-r v-center'>
                        <img src={chat_ico} alt=""/>
                    </a>
                    <a href="" className='flex-r v-center'>
                        <img src={notif_ico} alt=""/>
                    </a>
                    <a href="" className='flex-r v-center t-500 t-14'>
                        <img src={wallet_ico} alt="" className='with-title'/>
                        Connect Wallet
                    </a>
                </div>
            </div>
        </div>
    );
}

export default Navbar;



